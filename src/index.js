import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import configureAxios from './common/axios-config';

configureAxios();

ReactDOM.render(<App />, document.getElementById('root'));
