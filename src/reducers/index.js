import {
  ADD_PRODUCT_SUCCESS,
  CHANGE_PRODUCT_QUANTITY_SUCCESS,
  DELETE_PRODUCT_SUCCESS,
  ERROR,
  LOADING,
  GET_PRODUCTS_SUCCESS,
  SIGN_IN_SUCCESS,
  SIGN_IN_ERROR,
  SIGN_UP_SUCCESS,
  SIGN_UP_ERROR
} from '../action';

export default function productsReducer(
  state = { products: [], loading: false, isError: null },
  action
) {
  switch (action.type) {
    case ADD_PRODUCT_SUCCESS:
      return {
        ...state,
        products: [...state.products, { ...action.product }]
      };

    case CHANGE_PRODUCT_QUANTITY_SUCCESS:
      return {
        ...state,
        products: state.products.map(product => {
          if (product.id !== action.id) {
            return product;
          }

          product.quantity = action.value;

          return product;
        })
      };

    case DELETE_PRODUCT_SUCCESS:
      return { ...state, products: state.products.filter(product => product.id !== action.id) };

    case GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.products
      };

    case ERROR:
      return { ...state, isError: !!action.value, loading: false };

    case LOADING:
      return {
        ...state,
        loading: action.value,
        isSignUpError: false,
        isSignInError: false,
        isError: false
      };

    case SIGN_IN_SUCCESS:
    case SIGN_UP_SUCCESS:
      return { ...state, user: action.value.user };

    case SIGN_IN_ERROR:
      return { ...state, isSignInError: !!action.value, loading: false };

    case SIGN_UP_ERROR:
      return { ...state, isSignUpError: !!action.value, loading: false };

    default:
      return state;
  }
}
