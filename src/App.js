import React from 'react';
import ProductsContainer from './containers/ProductsContainer';
import productsReducer from './reducers';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Route, Router, Switch } from 'react-router-dom';
import PrivateRoute from './containers/PrivateRoute';
import LoginContainer from './containers/LoginContainer';
import RegistrationContainer from './containers/RegistrationContainer';
import history from './common/history';

const store = createStore(productsReducer, composeWithDevTools(applyMiddleware(thunk)));

const App = () => (
  <Router history={history}>
    <Provider store={store}>
      <Switch>
        <Route path="/registration" component={RegistrationContainer} />
        <PrivateRoute path="/cart/" component={ProductsContainer} />
        <Route path="/" component={LoginContainer} />
      </Switch>
    </Provider>
  </Router>
);
export default App;
