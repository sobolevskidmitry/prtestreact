import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Title, InputField, Counter, Button, ImageSelector } from '../components';

const Card = styled.div.attrs({
  className: 'card'
})`
  margin: 2rem 0 2rem 2rem;
  align-self: flex-start;
`;

class AddToList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newProduct: {
        name: '',
        price: null,
        quantity: 1,
        image: null
      },
      popupVisible: false
    };
  }

  fieldChange = (value, fieldName) =>
    this.setState(state => ({ newProduct: { ...state.newProduct, [fieldName]: value } }));

  togglePopup = () => {
    this.setState(() => ({ popupVisible: !this.state.popupVisible }));
  };

  selectImage = image => {
    this.fieldChange(image, 'image');
    this.togglePopup();
  };

  render() {
    const { newProduct, popupVisible } = this.state;
    const { addNewProduct } = this.props;

    return (
      <Card>
        <div className="card-header">
          <Title title="Add product to your cart list" />
        </div>
        <div className="card-body text-center">
          <InputField
            placeholder="Product name"
            value={newProduct.name}
            id="name"
            onChange={this.fieldChange}
          />
          <InputField
            placeholder="Product price"
            value={newProduct.price}
            id="price"
            onChange={this.fieldChange}
            type="number"
          />
          <Counter
            counter={newProduct.quantity}
            counterChange={e => this.fieldChange(e, 'quantity')}
          />
          <ImageSelector
            showPopup={this.togglePopup}
            popupVisible={popupVisible}
            selectImage={this.selectImage}
            selectedImage={newProduct.image}
          />
          <Button
            name="Add to list"
            handler={() => addNewProduct({ ...newProduct })}
            isDisabled={!(newProduct.name && newProduct.price && newProduct.image)}
          />
        </div>
      </Card>
    );
  }
}

AddToList.propTypes = {
  addNewProduct: PropTypes.func
};

export default AddToList;
