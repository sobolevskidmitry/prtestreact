import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { findById } from '../common/utils';
import { Product, ProductList } from '../components/index';
import AddToList from './AddToList';
import * as Actions from '../action';

const Wrapper = styled.div`
  display: flex;
  max-height: 100vh;
`;

const mapStateToProps = state => ({
  products: state.products,
  loading: state.loading
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ ...Actions }, dispatch),
  dispatch
});

class ProductsContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, getProducts } = this.props;
    dispatch(getProducts);
  }

  render() {
    const { products, addProduct, changeProductQuantity, deleteProduct, loading } = this.props;

    return (
      <Wrapper>
        <AddToList addNewProduct={addProduct} />
        <Switch>
          <Route
            exact
            path="/cart/"
            render={() => (
              <ProductList
                deleteProduct={deleteProduct}
                changeProductCount={changeProductQuantity}
                products={products}
                loading={loading}
              />
            )}
          />
          <Route
            path="/cart/:id"
            render={({ match }) => <Product {...findById(products, match.params.id)} />}
          />
        </Switch>
      </Wrapper>
    );
  }
}

ProductsContainer.propTypes = {
  getProducts: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
  addProduct: PropTypes.func.isRequired,
  changeProductQuantity: PropTypes.func.isRequired,
  deleteProduct: PropTypes.func.isRequired,
  loading: PropTypes.bool
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProductsContainer)
);
