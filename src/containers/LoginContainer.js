import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../action';
import Login from '../components/Login';

const mapStateToProps = state => ({
  loading: state.loading,
  isError: state.isSignInError
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ ...Actions }, dispatch),
  dispatch
});

class LoginContainer extends Component {
  constructor(props) {
    super(props);
  }

  submit = (email, password) => {
    const { signIn, dispatch } = this.props;
    dispatch(signIn(email, password));
  };

  render() {
    return <Login {...this.props} submit={this.submit} />;
  }
}

LoginContainer.propTypes = {
  signIn: PropTypes.func,
  loading: PropTypes.bool,
  isError: PropTypes.bool
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);
