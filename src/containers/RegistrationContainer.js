import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../action';
import Registration from '../components/Registration';

const mapStateToProps = state => ({
  loading: state.loading,
  isError: state.isSignUpError
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ ...Actions }, dispatch),
  dispatch
});

class RegistrationContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { signUp, dispatch } = this.props;

    return (
      <Registration
        {...this.props}
        signUp={(email, password) => dispatch(signUp(email, password))}
      />
    );
  }
}

RegistrationContainer.propTypes = {
  signUp: PropTypes.func,
  loading: PropTypes.bool,
  isError: PropTypes.bool
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationContainer);
