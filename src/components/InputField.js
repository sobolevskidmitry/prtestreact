import React from 'react';
import PropTypes from 'prop-types';

const InputField = ({ value = '', onChange, id, placeholder, type }) => (
  <p>
    <input
      type={type || 'text'}
      defaultValue={value}
      onChange={e => (onChange ? onChange(e.target.value, id) : '')}
      id={id}
      placeholder={placeholder}
      className="form-control"
    />
  </p>
);
InputField.propTypes = {
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  id: PropTypes.string,
  type: PropTypes.string
};

export default InputField;
