import React, { useState } from 'react';
import InputField from '../components/InputField';
import Button from '../components/Button';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as EmailValidator from 'email-validator';

const Card = styled.div.attrs({
  className: 'card'
})`
  margin: 7rem auto;
  width: 25rem;

  .card-header {
    background: #fff;
    text-align: right;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
  }
`;

const Error = styled.span`
  margin: 1rem;
  color: red;
`;

const Registration = props => {
  const { loading, signUp, isError } = props;
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const passwordEquals = password === confirmPassword;
  const emailValid = EmailValidator.validate(email);

  return (
    <Card>
      <div className="card-body">
        <div className="card-header">
          <h4>Sign up</h4>
          <Link to={'/'}>{'Sign in'}</Link>
        </div>
        <InputField placeholder={'Email'} onChange={setEmail} />
        <InputField placeholder={'Password'} onChange={setPassword} type={'password'} />
        <InputField
          placeholder={'Repeat Password'}
          onChange={setConfirmPassword}
          type={'password'}
        />
        {loading ? (
          'Loading'
        ) : (
          <Button
            name={'Sign up'}
            handler={() => signUp(email, password)}
            isDisabled={!passwordEquals || !password || !confirmPassword || !emailValid}
          />
        )}
        {isError ? <Error>Try again</Error> : ''}
        {!passwordEquals && confirmPassword ? <Error>Passwords should match</Error> : ''}
        {email && email.length && !emailValid ? <Error>Invalid email</Error> : ''}
      </div>
    </Card>
  );
};

Registration.propTypes = {
  signUp: PropTypes.func,
  loading: PropTypes.bool,
  isError: PropTypes.bool
};

export default Registration;
