import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ name, handler, isDisabled = false, type = 'primary' }) => (
  <button onClick={handler} disabled={isDisabled} className={`btn btn-outline-${type}`}>
    {name}
  </button>
);

Button.propTypes = {
  name: PropTypes.string,
  handler: PropTypes.func,
  isDisabled: PropTypes.bool,
  type: PropTypes.string
};

export default Button;
