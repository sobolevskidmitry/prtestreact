import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ProductTotal from './ProductTotal';

const Ul = styled.ul`
  padding: 0;
  li {
    list-style: none;
  }
`;

const ProductDesc = ({ quantity, price }) => (
  <Ul>
    <li>Count: {quantity}</li>
    <li>Price: {price} $</li>
    <li>
      <b>
        <ProductTotal price={price} quantity={quantity} />
      </b>
    </li>
  </Ul>
);

ProductDesc.propTypes = {
  quantity: PropTypes.number,
  price: PropTypes.number
};

export default ProductDesc;
