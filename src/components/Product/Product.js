import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Image, ProductDesc, Button, Title } from '../index';
import { Link } from 'react-router-dom';

const Card = styled.div.attrs({
  className: 'card'
})`
  margin: 2rem;
  width: 30vw;

  .card-body {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`;

const Product = ({ name, price, quantity, image }) => (
  <Card>
    <div className="card-header">
      <Title title={name} />
    </div>
    <div className="card-body">
      <Image image={image} />
      <ProductDesc quantity={quantity} price={price} />
      <Link to="/cart/">
        <Button name="Back to list" />
      </Link>
    </div>
  </Card>
);

Product.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  cost: PropTypes.number,
  price: PropTypes.number,
  quantity: PropTypes.number,
  image: PropTypes.string
};

export default Product;
