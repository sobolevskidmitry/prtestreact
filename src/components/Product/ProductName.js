import React from 'react';
import PropTypes from 'prop-types';

const ProductName = ({ name, handler }) => <h4 onClick={handler}>{name}</h4>;

ProductName.propTypes = {
  name: PropTypes.string,
  handler: PropTypes.func
};

export default ProductName;
