import React from 'react';
import PropTypes from 'prop-types';

const ProductTotal = ({ price, quantity }) => <span>Total: {price * quantity} $</span>;

ProductTotal.propTypes = {
  price: PropTypes.number,
  quantity: PropTypes.number
};

export default ProductTotal;
