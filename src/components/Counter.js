import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CounterLabel = styled.div`
  margin: 0.5rem;
  display: inline-block;
`;

const CounterBtn = styled.button.attrs({
  className: 'btn btn-outline-dark btn-sm'
})`
  width: 2rem;
`;

const Counter = ({ counter = 1, counterChange }) => {
  const increaseCounter = () => {
    const curValue = counter + 1;

    counterChange(curValue);
  };

  const decreaseCounter = () => {
    if (counter === 1) {
      return;
    }
    const curValue = counter - 1;

    counterChange(curValue);
  };

  return (
    <div>
      <CounterBtn onClick={decreaseCounter}>-</CounterBtn>
      <CounterLabel>{counter}</CounterLabel>
      <CounterBtn onClick={increaseCounter}>+</CounterBtn>
    </div>
  );
};

Counter.propTypes = {
  counter: PropTypes.number,
  counterChange: PropTypes.func
};

export default Counter;
