import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledImage = styled.img`
  max-width: 10rem;
  max-height: 5rem;
  cursor: pointer;
  border-radius: 50%;
`;

const Image = ({ image, handler, name }) => (
  <StyledImage src={image} alt={name} onClick={handler} />
);

Image.propTypes = {
  image: PropTypes.string,
  handler: PropTypes.func,
  name: PropTypes.string
};

export default Image;
