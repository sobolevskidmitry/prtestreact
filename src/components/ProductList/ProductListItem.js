import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Image, ProductTotal, ProductName, Counter, IconBtn } from '..';
import { Link } from 'react-router-dom';

const ListGroupItem = styled.div.attrs({
  className: 'list-group-item'
})`
  display: flex;
  position: relative;
  justify-content: space-between;
  margin: 1rem;
  min-height: 8rem;

  .counter-wrapper {
    text-align: center;
  }
`;

const ProductListItem = ({
  name,
  price,
  image,
  id,
  quantity,
  changeProductCount,
  deleteProduct
}) => (
  <ListGroupItem>
    <Link to={`/cart/${id}`}>
      <Image image={image} />
    </Link>
    <div className="counter-wrapper">
      <ProductName name={name} />
      <Counter counter={quantity} counterChange={e => changeProductCount(id, e)} />
      <ProductTotal quantity={quantity} price={price} />
    </div>
    <div className="links">
      <IconBtn title="Delete" handler={() => deleteProduct(id)}>
        <i className="far fa-trash-alt" />
      </IconBtn>
      <Link to={`/cart/${id}`}>
        <IconBtn title="Go to product">
          <i className="fas fa-link" />
        </IconBtn>
      </Link>
    </div>
  </ListGroupItem>
);

ProductListItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string,
  id: PropTypes.string.isRequired,
  quantity: PropTypes.number,
  changeProductCount: PropTypes.func,
  deleteProduct: PropTypes.func,
  toProductHandler: PropTypes.func,
  viewProduct: PropTypes.func
};

export default ProductListItem;
