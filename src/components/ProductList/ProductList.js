import React from 'react';
import * as PropTypes from 'prop-types';
import styled from 'styled-components';
import { ProductListItem, Title } from '../index';

const Card = styled.div.attrs({
  className: 'card'
})`
  margin: 2rem;
  width: 30vw;

  .list-group.card-body {
    padding: 1.25rem;
    overflow-y: auto;
  }
`;

const calculateTotalPrice = products =>
  products.length
    ? products.reduce((acc, product) => acc + +product.price * +product.quantity, 0)
    : 0;

const ProductList = ({ products, deleteProduct, changeProductCount, loading }) => (
  <Card>
    <div className="card-header">
      <Title title="Product List" />
    </div>
    <div className="list-group card-body">
      {products.length ? (
        products.map(product => (
          <ProductListItem
            {...product}
            key={product.id}
            changeProductCount={changeProductCount}
            deleteProduct={deleteProduct}
          />
        ))
      ) : loading ? (
        <span>Loading...</span>
      ) : (
        <div className="text-center">Add some products, please</div>
      )}
    </div>
    <div className="card-footer text-muted">Total: {calculateTotalPrice(products)} $</div>
  </Card>
);

ProductList.propTypes = {
  products: PropTypes.array.isRequired,
  deleteProduct: PropTypes.func,
  changeProductCount: PropTypes.func,
  loading: PropTypes.bool
};

export default ProductList;
