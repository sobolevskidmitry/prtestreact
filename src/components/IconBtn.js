import React from 'react';
import PropTypes from 'prop-types';

const IconBtn = ({ title, handler, children }) => (
  <button title={title} onClick={handler} className="btn-link btn">
    {children}
    <span className="glyphicon glyphicon-star" aria-hidden="true" />
  </button>
);

IconBtn.propTypes = {
  title: PropTypes.string,
  handler: PropTypes.func,
  children: PropTypes.object
};

export default IconBtn;
