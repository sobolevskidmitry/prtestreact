import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Image } from '.';
import DefaultImg from '../assets/images/question.png';
import BananaImg from '../assets/images/banana.jpg';
import OrangeImg from '../assets/images/orange.jpg';
import MelonImg from '../assets/images/melon.jpg';
import GrapeImg from '../assets/images/grape.jpg';

const ImageSelectorWrapper = styled.div`
  position: relative;
  padding: 0.5rem;
`;

const PopUp = styled.div`
  position: absolute;
  display: flex;
  background: white;
  z-index: 1;
  border: 1px solid lightgrey;
  border-radius: 4px;
  padding: 1rem;
  left: 50%;
  transform: translateX(-50%);

  > ${Image}:hover {
    filter: grayscale(40%);
  }
`;

const mockImages = [
  { id: 1, url: BananaImg },
  { id: 2, url: OrangeImg },
  { id: 3, url: MelonImg },
  { id: 4, url: GrapeImg }
];

const ImageSelector = ({
  images = mockImages,
  selectImage,
  selectedImage,
  popupVisible,
  showPopup
}) => (
  <ImageSelectorWrapper>
    <Image image={selectedImage || DefaultImg} handler={showPopup} />
    {popupVisible ? (
      <PopUp>
        {images.map(image => (
          <Image image={image.url} handler={() => selectImage(image.url)} key={image.id} />
        ))}
      </PopUp>
    ) : null}
  </ImageSelectorWrapper>
);

ImageSelector.propTypes = {
  selectImage: PropTypes.func,
  images: PropTypes.arrayOf(PropTypes.string),
  selectedImage: PropTypes.string,
  popupVisible: PropTypes.bool,
  showPopup: PropTypes.func
};

export default ImageSelector;
