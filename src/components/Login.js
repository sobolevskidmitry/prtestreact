import React, { useState } from 'react';
import InputField from '../components/InputField';
import Button from '../components/Button';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import * as EmailValidator from 'email-validator';

const Card = styled.div.attrs({
  className: 'card'
})`
  margin: 7rem auto;
  width: 25rem;

  .card-header {
    background: #fff;
    text-align: right;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
  }
`;

const Error = styled.span`
  margin: 1rem;
  color: red;
`;
const Login = props => {
  const { loading, submit, isError } = props;
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const emailValid = EmailValidator.validate(email);

  return (
    <Card>
      <div className="card-body">
        <div className="card-header">
          <h4>Sign in</h4>
          <Link to={'/registration'}>{'Sign up'}</Link>
        </div>

        <InputField placeholder={'Email'} onChange={setEmail} type={'email'} />
        <InputField type={'password'} placeholder={'Password'} onChange={setPassword} />
        {loading ? (
          'Loading'
        ) : (
          <Button
            name={'Sign in'}
            handler={() => submit(email, password)}
            isDisabled={!emailValid}
          />
        )}
        {isError ? <Error>Try again</Error> : ''}
        {email && email.length && !emailValid ? <Error>Invalid email</Error> : ''}
      </div>
    </Card>
  );
};

Login.propTypes = {
  submit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  isError: PropTypes.bool
};

export default Login;
