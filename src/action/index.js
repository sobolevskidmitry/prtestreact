import axios from 'axios';
import { Product } from '../models/Product';
import { apiConfig } from '../common/api-config';
import history from '../common/history';
import { setUserData } from '../common/utils';

export const ADD_PRODUCT_SUCCESS = 'ADD_PRODUCT_SUCCESS';
export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const CHANGE_PRODUCT_QUANTITY_SUCCESS = 'CHANGE_PRODUCT_QUANTITY_SUCCESS';
export const LOADING = 'LOADING';
export const ERROR = 'ERROR';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_ERROR = 'SIGN_IN_ERROR';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_ERROR = 'SIGN_UP_ERROR';

export const loadingToggle = value => ({ type: LOADING, value });
export const error = value => ({ type: ERROR, value });

export const addProductSuccess = product => ({ type: ADD_PRODUCT_SUCCESS, product });
export const changeProductQuantitySuccess = (id, value) => ({
  type: CHANGE_PRODUCT_QUANTITY_SUCCESS,
  id,
  value
});
export const deleteProductSuccess = id => ({ type: DELETE_PRODUCT_SUCCESS, id });
export const getProductsSuccess = products => ({ type: GET_PRODUCTS_SUCCESS, products });
export const signInSuccess = value => ({ type: SIGN_IN_SUCCESS, value });
export const signInError = value => ({ type: SIGN_IN_ERROR, value });
export const signUpSuccess = value => ({ type: SIGN_UP_SUCCESS, value });
export const signUpError = value => ({ type: SIGN_UP_ERROR, value });

export const addProduct = product => dispatch => {
  dispatch(loadingToggle(true));

  axios
    .post(apiConfig.addProduct(), product)
    .then(response => {
      if (!response || !response.data) {
        throw Error('Api Error');
      }
      dispatch(loadingToggle(false));

      return new Product(response.data);
    })
    .then(product => dispatch(addProductSuccess(product)))
    .catch(errorMessage => dispatch(error(errorMessage)));
};

export const changeProductQuantity = (id, value) => dispatch => {
  dispatch(loadingToggle(true));

  axios
    .put(
      apiConfig.changeProductQuantity(id),
      { quantity: value },
      {
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json'
        }
      }
    )
    .then(response => {
      if (!response || !response.data) {
        throw Error('Api Error');
      }
      dispatch(loadingToggle(false));

      return new Product(response.data);
    })
    .then(product => dispatch(changeProductQuantitySuccess(product.id, product.quantity)))
    .catch(errorMessage => dispatch(error(errorMessage)));
};

export const deleteProduct = id => dispatch => {
  dispatch(loadingToggle(true));

  axios
    .post(apiConfig.deleteProduct(id))
    .then(response => {
      if (!response || !response.data) {
        throw Error('Api Error');
      }
      dispatch(loadingToggle(false));

      return new Product(response.data);
    })
    .then(product => dispatch(deleteProductSuccess(product.id)))
    .catch(errorMessage => dispatch(error(errorMessage)));
};

export const getProducts = () => dispatch => {
  dispatch(loadingToggle(true));

  axios
    .get(apiConfig.getProducts())
    .then(response => {
      if (!response || !response.data) {
        throw Error('Api Error');
      }

      dispatch(loadingToggle(false));

      return response.data.map(product => new Product(product));
    })
    .then(products => dispatch(getProductsSuccess(products)))
    .catch(errorMessage => dispatch(error(errorMessage)));
};

export const signIn = (email, password) => dispatch => () => {
  dispatch(loadingToggle(true));
  axios
    .post(apiConfig.signIn(), { user: { email, password } })
    .then(response => {
      if (!response || !response.data) {
        throw Error('Api Error');
      }

      dispatch(loadingToggle(false));
      return response;
    })
    .then(value => {
      dispatch(signInSuccess(value.data));
      setUserData(value.data.user);
      history.push('/cart/');
    })
    .catch(errorMessage => {
      dispatch(signInError(errorMessage));
    });
};

export const signUp = (email, password) => dispatch => () => {
  dispatch(loadingToggle(true));
  axios
    .post(apiConfig.signUp(), { user: { email, password } })
    .then(response => {
      if (!response || !response.data) {
        throw Error('Api Error');
      }

      dispatch(loadingToggle(false));
      return response;
    })
    .then(value => {
      dispatch(signUpSuccess(value.data));
      setUserData(value.data.user);
      history.push('/cart/');
    })
    .catch(errorMessage => {
      dispatch(signUpError(errorMessage));
    });
};
