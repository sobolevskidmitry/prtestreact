export class Product {
  constructor({ _id, name, image, price, quantity }) {
    this.id = _id;
    this.name = name;
    this.image = image;
    this.price = price;
    this.quantity = quantity;
  }
}
