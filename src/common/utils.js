export const findById = (arr, id) => arr.find(element => element.id === id);

const userSessionStorageKey = 'user';

export const setUserData = user => {
  try {
    sessionStorage.setItem(userSessionStorageKey, JSON.stringify({ ...user }));
  } catch (e) {
    // eslint-disable-next-line
    console.log(e);
  }
};

export const getUserData = () => {
  try {
    return JSON.parse(sessionStorage.getItem(userSessionStorageKey));
  } catch (e) {
    // eslint-disable-next-line
    console.log(e);
  }
};

export const deleteUserData = () => {
  try {
    sessionStorage.removeItem(userSessionStorageKey);
  } catch (e) {
    // eslint-disable-next-line
    console.log(e);
  }
};
