export const baseUrl = ' http://192.168.14.18:3001';

export const apiConfig = {
  addProduct: () => `${baseUrl}/products`,
  changeProductQuantity: id => `${baseUrl}/products/${id}`,
  deleteProduct: id => `${baseUrl}/products/${id}/remove`,
  getProducts: () => `${baseUrl}/products`,
  signIn: () => `${baseUrl}/user/login`,
  signUp: () => `${baseUrl}/user`
};
