import axios from 'axios';
import { deleteUserData, getUserData } from './utils';
import history from './history';

function attachToken(config) {
  const user = getUserData();

  config.headers = user && user.token ? { Authorization: user.token } : { Authorization: '' };
}

export default function configureAxios() {
  axios.defaults.headers.post['Content-Type'] = 'application/json';

  axios.interceptors.request.use(request => {
    attachToken(request);
    return request;
  });

  axios.interceptors.response.use(
    response => response,
    error => {
      if (error) {
        deleteUserData();
        history.push('/');
      }

      return Promise.reject(error);
    }
  );
}
